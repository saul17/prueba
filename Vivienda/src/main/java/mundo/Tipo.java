/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mundo;

/**
 *Describe el tipo al que pertence el producto según su fin o 
 * grupo alimenticio
 * @author HP 14
 */
public enum Tipo {
    HIGUIENE, LIMPIEZA, CARNES, VEGETALES, FRUTAS,
    ESPECIES, BEBIDAS, HARINA_GRANOS, OTROS;
}
