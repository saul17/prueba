/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mundo;

/**
 *Define en que lugar de la vivienda se guardan los productos
 * @author HP 14
 */
public enum Lugar {
    ESTANTES, COCINA, REFRIGERADOR, DORMITORIO;
}
