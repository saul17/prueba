/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mundo;

import java.util.ArrayList;

/**
 *
 * @author HP 14
 */
public class Producto {
    //Atributos de la clase
    private String nombre;
    private int cExistente;
    private int cMinima;
    private float precioUni;
    
    //Asociaciones de otras clases o enumeraciones
    private Tipo tipo;
    private Lugar lugar;
    private Presentacion presentacion;
    private ArrayList<Transaccion> transacciones;
    
    //Metodos
    //Constructor
    public Producto(String nombre, int cExistente, int cMinima, float precioUni, Tipo tipo, Lugar lugar) {
        this.nombre = nombre;
        this.cExistente = cExistente;
        this.cMinima = cMinima;
        this.precioUni = precioUni;
        this.tipo = tipo;
        this.lugar = lugar;
    }
    
    //Getters and setters

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getcExistente() {
        return cExistente;
    }

    public void setcExistente(int cExistente) {
        this.cExistente = cExistente;
    }

    public int getcMinima() {
        return cMinima;
    }

    public void setcMinima(int cMinima) {
        this.cMinima = cMinima;
    }

    public float getPrecioUni() {
        return precioUni;
    }

    public void setPrecioUni(int precioUni) {
        this.precioUni = precioUni;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Lugar getLugar() {
        return lugar;
    }

    public void setLugar(Lugar lugar) {
        this.lugar = lugar;
    }

    public Presentacion getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(Presentacion presentacion) {
        this.presentacion = presentacion;
    }

    public ArrayList<Transaccion> getTransacciones() {
        return transacciones;
    }

    public void setTransacciones(ArrayList<Transaccion> transacciones) {
        this.transacciones = transacciones;
    }

    @Override
    public String toString() {
        return "Producto{" + "nombre=" + nombre + ", cExistente=" + cExistente + ", cMinima=" + cMinima + ", precioUni=" + precioUni + ", tipo=" + tipo + ", lugar=" + lugar + '}';
    }
    
    
}
